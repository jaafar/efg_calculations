from setuptools import find_packages, setup

setup(
    name='efg_calcs',
    version='0.1.0',
    description='Calculation of electric field gradients using density functional theory in order to study materials of interest via nuclear quadrupole resonance spectroscopy.',
    author='Jaafar Ansari',
    license='MIT',
    package_dir= {'': 'src'},
    packages=find_packages()
)
