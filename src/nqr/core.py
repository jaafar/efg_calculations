#!/usr/bin/env python3
from fractions import Fraction
import numpy as np
import numpy.linalg as la
import os.path as op
import pandas as pd
from scipy.constants import e, h
from utils.utils import ROOT

class Nqr:
    def __init__(self, mat, site, isotope, spin,
                 Q=None, nuQ=None, eta=None, freqs=None, efg=None):
        self.mat = mat
        self.site = site
        self.Z = isotope
        self.I = spin
        self.Q = Q if Q != None else get_Q(site)[tuple([site, self.Z])]
        self.nuQ = nuQ
        self.eta = eta
        self.freqs = freqs
        self.efg = efg

    @classmethod
    def from_freqs(cls, mat, site, isotope, spin, freqs):
        components = np.array([tensor[0, 0], tensor[1,1], tensor[2,2]])
        sort_indices = np.argsort(np.abs(components))
        V = components[sort_indices]

        return cls(mat, site, isotope, spin)


    def get_H(self, eta=None):
        """
        Electric quadrupolar Hamiltonian,
        normalized with respect to 3*e*V_zz*Q/(4*I*(2*I-1)*h) [Suits].

        Args:
            I (float): spin
            eta (np.ndarray): 1D range of values from 0 to 1

        Returns:
            np.ndarray: Array of Hamiltonians, each corresponding
            to a single eta value.
        """

        if eta == None:
            eta = np.array(self.eta)
        else:
            eta = np.array(eta)
        eta = eta.reshape(eta.size, 1, 1) # Reshape to permit broadcasting
        I = self.I
        Ix, Iy, Iz = get_spin_matrices(I)
        identity = np.identity(int(2*I + 1))
        factor = e*self.Q/(4*I*(2*I - 1))
        H = h/3*(3*Iz@Iz - I*(I+1)*identity + eta*(Ix@Ix - np.real(Iy@Iy)))

        return H

    def __str__(self):
        out = ''
        out += 'material: {}\n'.format(self.mat)
        out += 'site: {}-{}\n'.format(self.site, self.Z)
        out += 'spin: {}\n'.format(Fraction(self.I))
        out += 'quadrupole moment: {} mb\n'.format(self.Q)
        out += 'quadrupole coupling constant: {} MHz\n'.format(self.nuQ)\
               if self.nuQ != None else ''
        out += 'eta: {}\n'.format(self.eta) if self.eta != None else ''
        out += 'frequencies: {} MHz\n'.format(self.freqs)\
               if self.freqs != None else ''

        return out

class Efg:
    def __init__(self, mat, site, V=None, pac_tensor=None, gac_tensor=None):
        self.mat = mat
        self.site = site
        self._pac_tensor = pac_tensor
        self._gac_tensor = gac_tensor
        self.V = np.array(V)
        self.Vxx = self.V[..., 0]
        self.Vyy = self.V[..., 1]
        self.Vzz = self.V[..., 2]

    @property
    def eta(self):
        return (self.Vxx - self.Vyy)/self.Vzz

    @property
    def pac_tensor(self):
        return self._pac_tensor

    @pac_tensor.setter
    def pac_tensor(self, tensor):
        self._pac_tensor = tensor

    @classmethod
    def from_pac_tensor(cls, mat, site, tensor):
        components = np.array([tensor[0, 0], tensor[1,1], tensor[2,2]])
        sort_indices = np.argsort(np.abs(components))
        V = components[sort_indices]

        return cls(mat, site, V=V, pac_tensor=tensor)

    @property
    def gac_tensor(self):
        return self._gac_tensor

    @gac_tensor.setter
    def gac_tensor(self, tensor):
        self._gac_tensor = tensor

    @classmethod
    def from_gac_tensor(cls, mat, site, tensor):
        # develop later
        pass

    def __neg__(self, other):
        return self.__class__(self.mat, self.site, -1*self.V,
                              self.pac_tensor, self.gac_tensor)

    def __mul__(self, other):
        return self.__class__(self.mat, self.site, other*self.V,
                              self.pac_tensor, self.gac_tensor)

    def __rmul__(self, other):
        return self*other

    def __truediv__(self, other):
        return self.__class__(self.mat, self.site, self.V/other,
                              self.pac_tensor, self.gac_tensor)

    def __repr__(self):
        return f'Efg({self.mat}, {self.site}, {self.V/1e21})'

    def __str__(self):
        return f'{self.V/1e21}'

def get_spin_matrices(I):
    # Reference: https://en.wikipedia.org/wiki/Spin_(physics)#Higher_spins
    n = int(2*I + 1)
    a, b = np.indices((n, n)) + 1
    del1 = (a == b + 1).astype(np.int)
    del2 = (a + 1 == b).astype(np.int)
    del3 = (a == b).astype(np.int)
    sqrt_term = np.sqrt((I + 1)*(a + b - 1) - a*b)

    Ix = (del1 + del2)*sqrt_term/2
    Iy = 1j*(del1 - del2)*sqrt_term/2
    Iz = (I + 1 - a)*del3

    return Ix, Iy, Iz

def get_norm_H(I, eta):
    """
    Electric quadrupolar Hamiltonian,
    normalized with respect to 3*e*V_zz*Q/(4*I*(2*I-1)*h) [Suits].

    Args:
        I (float): spin
        eta (np.ndarray): 1D range of values from 0 to 1

    Returns:
        np.ndarray: Array of Hamiltonians, each corresponding
        to a single eta value.
    """

    eta = np.array(eta)
    eta = eta.reshape(eta.size, 1, 1) # Reshape to permit broadcasting
    Ix, Iy, Iz = get_spin_matrices(I)
    identity = np.identity(int(2*I + 1))
    H = h/3*(3*Iz@Iz - I*(I+1)*identity + eta*(Ix@Ix - np.real(Iy@Iy)))

    return H

def get_hamiltonian(I, Q, V):
    Vxx, Vyy, Vzz = V[0], V[1], V[2]
    Ix, Iy, Iz = get_spin_matrices(I)
    identity = np.identity(len(Iz))
    Ix2 = np.matmul(Ix, Ix)
    Iy2 = np.real(np.matmul(Iy, Iy))
    Iz2 = np.matmul(Iz, Iz)
    H = e*Q/(4*I*(2*I-1))*(Vzz*(3*Iz2 - I*(I+1)*identity) + (Vxx - Vyy)*(Ix2 - Iy2))

    return H

def get_freqs(H):
    E, _ = la.eig(H)
    E_diff = np.sort(np.diff(np.sort(np.real(E))))
    f = E_diff/h

    return f[f > 1]

def get_Q(*args):
    """
    Get nuclear quadrupole moment value in mb (1e-28 m^2),
    from Pekka Pyykkö year 2017 data
    [https://doi.org/10.1080/00268976.2018.1426131].

    Args:
        Give any number of nuclei and associated isotopes.
        Nucleus must be string, isotopes can be int or list of ints or None.
        If None, all isotopes are returned.
        E.g,: get_Q('N', 14) and get_Q('N') return the same Qs.
              get_Q('N', 'Cl', [35, 37]) and get_Q('N', 'Cl') return same Qs
              get_Q('N', 'Cl') returns all Qs for all isotopes

    Returns:
        Dictionary with keys in the form of (nucleus, isotope)
    """
    quad_mom_path = op.join(ROOT, 'references', 'quad_mom')
    csv = op.join(quad_mom_path, 'quad_mom_2017.csv')
    df = pd.read_csv(csv, index_col=['nucleus', 'isotope'])

    # build list of indices
    indices = []
    for i, arg in enumerate(args):
        if type(arg) == int:
            continue
        if arg != args[-1]:
            if type(args[i+1]) != str:
                isos = args[i+1]
                if type(isos) != list:
                    isos = [isos]
                for x in isos:
                    indices.append(tuple([args[i], x]))
            else:
                indices.append(args[i])
        else:
            indices.append(args[i])

    Qs = {}
    for i in indices:
        d = df.loc[[i], :]['q'].to_dict()
        Qs.update(d)

    return Qs

def _clean_Q_csv():
    quad_mom_path = op.join(ROOT, 'references', 'quad_mom')
    csv_in_path = op.join(quad_mom_path, 'original_quad_mom_2017.csv')
    csv_out_path = op.join(quad_mom_path, 'quad_mom_2017.csv')

    raw = pd.read_csv(csv_in_path)
    df = [None]*3
    df[0] = raw.iloc[:, 0:3]
    df[1] = raw.iloc[:, 3:6]
    df[2] = raw.iloc[:, 6:]
    df[1].columns = df[0].columns
    df[2].columns = df[0].columns
    df = pd.concat(df)

    # Remove empty rows
    df = df[df['Nucleus'].str.strip().astype(bool)]

    # Split nucleus column into seperate nucleus, istope, and excited columns
    new_cols = df['Nucleus'].str.extract(r'([a-zA-Z]+)-([\d]+)(\*)*')
    df['Nucleus'] = new_cols[0]
    df['isotope'] = new_cols[1]
    df['excited'] = new_cols[2].replace(r'\*', True, regex=True)

    # Create "new values" column
    new_cols = df['Q'].str.split('•', n=1).str
    df['Q'], df['new'] = new_cols[0], new_cols[1]
    df.fillna(value=False, inplace=True)
    df['new'].replace(r'\s*', True, regex=True, inplace=True)

    # Separate value and uncertainies into 2 columns
    df['uncert'] = df['Q'].str.extract(r'\((\d*)\)')
    df['Q'] = df['Q'].str.extract(r'([−\.\d]+)(?:\(\d*\))*')
    df['Q'].replace(r'−', '-', regex=True, inplace=True)

    # Add name to 'methods' column
    df = df.rename(columns={df.columns[2]: 'methods'})

    # Reorder columns and make lowercase
    cols = ['Nucleus', 'isotope', 'Q', 'uncert', 'new', 'excited', 'methods']
    df = df[cols]
    df = df.rename(str.lower, axis='columns')

    # Fix data types
    types_map = {'isotope': 'int64', 'q': 'float64', 'uncert': 'float64'}
    df = df.astype(types_map)

    # Reset index column
    df = df.reset_index(drop=True)

    df.to_csv(csv_out_path)

if __name__ == '__main__':
    TaP_nqr = Nqr('TaP', 'Ta', 181, 7/2)
    print(TaP_nqr.__dict__)
#    _clean_Q_csv()
#    Qs = get_Q('Ta')
#    print(Qs)
#    import timeit
#    mysetup = '''
#from __main__ import get_norm_H
#import numpy as np
#I = 7/2
#eta = np.linspace(0, 1, 1000000)'''
#    mycode2 = '''
#for i in np.linspace(0, 1, 1000000):
#    get_norm_H(I, i)'''
#    mycode1 = "get_norm_H(I, eta)"
#    time1 = timeit.timeit(stmt=mycode1, setup=mysetup, number=1)
#    time2 = timeit.timeit(stmt=mycode2, setup=mysetup, number=1)
#    print(time1, time2, np.abs(time2-time1)/time2)
