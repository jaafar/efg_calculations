#!/usr/bin/env python
from utils.utils import load_file
import re

class Scf:
    def __init__(self, path, all=False):
        self.path = path
        raw_scf = load_file(self.path)
        self._raw = ''

    @property
    def raw(self):
        return self._raw

    @raw.setter
    def raw(self, r):
        self._raw = r

    @property
    def header(self):
        if self.raw == '':
            self.raw = load_file(self.path)

        h_pattern = r'(.+)\s+\-+\s+:ITE001'
        hp = re.compile(h_pattern, re.DOTALL)
        h = re.search(hp, self.raw).group(1).strip()

        return h

    @property
    def iterations(self):
        if self.raw == '':
            self.raw = load_file(self.path)

        iter_pattern = r'(\s+\-+\s+:ITE.+?)(?=(\s+?\-+?\s+?:ITE)|$)'
        ip = re.compile(iter_pattern, re.DOTALL)
        iters = re.findall(ip, self.raw)
        iterations = [x[0] for x in iters]

        return iterations

