#!/usr/bin/env python
import glob
import numpy as np
from nqr.core import Efg
import os
import os.path as op
from pymatgen.io.qchem.utils import read_table_pattern
import re
from utils.utils import load_file, custom_regex_patterns
from wien2k.scf import Scf
from wien2k.warnings import Wien2kWarning
import warnings

class Wien2kParse:
    def __init__(self, case_path, conv_dir='convergence'):
        self.case_path = op.abspath(case_path)
        self.case = op.basename(self.case_path)
        self._check_error()
        self.get_in_params()
        self._conv_path = op.join(self.case_path, conv_dir)
        self._scf = Scf(self.get_file_path('scf'))

    def get_conv_paths(self, conv_param):
        path = op.join(self.conv_path, conv_param)
        dirs = next(os.walk(path))[1]
        paths = [op.join(path, x, self.case) for x in dirs]

        return paths

    @property
    def conv_path(self):
        return self._conv_path

    @conv_path.setter
    def conv_path(self, p):
        self._conv_path = op.abspath(p)

    @property
    def scf(self):
        return self._scf

    @property
    def species(self):
        pattern = r'ATOMNUMBER=\s+(\d+)\s([ a-zA-Z0-9]+)\s+'
        p = re.compile(pattern)
        final_iter = self.scf.iterations[-1]
        matches = re.findall(p, final_iter)
        sp = [x[1].strip() for x in matches]

        return sp

    def get_in_params(self):
        # RKMAX
        try:
            in1_c = load_file(self.get_file_path('in1c'))
        except:
            in1_c = load_file(self.get_file_path('in1'))
        pattern = r'(\s+\d+\.?\d*)(\s+\d+\.?\d*)(\s+\d+\.?\d*)'\
                  r'.+\(R\-MT\*K\-MAX,MAX L IN WF,V\-NMT,LIB\)'
        p = re.compile(pattern)
        self.rkmax = float(p.search(in1_c).group(1))

        # GMAX
        try:
            in2_c = load_file(self.get_file_path('in2c'))
        except:
            in2_c = load_file(self.get_file_path('in2'))
        pattern = r'(\d+\.?\d+)\s+GMAX'
        p = re.compile(pattern)
        self.gmax = float(p.search(in2_c).group(1))

        # Number of k-points
        klist = load_file(self.get_file_path('klist'))
        pattern = r'(\d+)\s+k'
        p = re.compile(pattern)
        self.kpoints = int(p.search(klist).group(1))

    @property
    def efg(self):
        V = {}
        flt = custom_regex_patterns('float')
        for i, s in enumerate(self.species):
            header_patt = rf'ATOMNUMBER=.+{self.species[i]}'\
                          + rf'.+?V21M\sTOT/SRF='\
                          + rf'\s+' + rf'({flt}\s*)'*2 + rf'\n\n'
            row_patt = rf'(\s+{flt})'*6 + rf'\n'
            footer_patt = r'(\n+\s+MAIN DIRECTIONS OF THE EFG)??'
            final_iter = self.scf.iterations[-1]
            table = read_table_pattern(final_iter,
                                       header_patt, row_patt,
                                       footer_patt, postprocess=float)
            tensor_nondiag = np.array(table[0])[:, 0:3]*1e21
            tensor_diag = np.array(table[0])[:, 3:]*1e21
            efg = Efg.from_pac_tensor(self.case, s,
                                      tensor=tensor_diag)
            efg.gac_tensor = tensor_diag=tensor_nondiag
            V[s] = efg

        return V

    def efg_conv_data(self, site, conv_param, conv_dir=None,
                      Vzz_tol=1e-4, eta_tol=0.01):
        if conv_dir == None:
            paths = self.get_conv_paths(conv_param)
        else:
            paths = self.get_conv_paths(conv_dir)
        X = np.zeros(len(paths))
        Vxx = np.zeros(len(paths))
        Vyy = np.zeros(len(paths))
        Vzz = np.zeros(len(paths))
        eta = np.zeros(len(paths))
        for i, path in enumerate(paths):
            wien2k = Wien2kParse(path, conv_dir='')
            X[i] = getattr(wien2k, conv_param)
            efg = wien2k.efg
            Vxx[i] = efg[site].Vxx
            Vyy[i] = efg[site].Vyy
            Vzz[i] = efg[site].Vzz
            eta[i] = efg[site].eta
        sort_indices = X.argsort()
        X = X[sort_indices]
        Vxx = Vxx[sort_indices]
        Vyy = Vyy[sort_indices]
        Vzz = Vzz[sort_indices]
        eta = eta[sort_indices]

        return X, Vxx, Vyy, Vzz, eta

    def check_conv(self, X, data, tol):
        diff = np.diff(data)
        for i, val in enumerate(diff):
            cond = np.abs(diff)[i:] < tol
            if np.abs(val) < tol and np.all(cond):
                return X[i+1]

    def _get_error_files(self):
        all_err_files = glob.glob(op.join(self.case_path, '*error*'))
        err_files = []
        for e in all_err_files:
            text = load_file(e)
            if text != '':
                err_file = op.relpath(e)
                err_files.append(err_file)

        return err_files

    def _check_error(self):
        err_files = self._get_error_files()
        if err_files != []:
            err_files_str = ''.join(err_files)
            message = 'Errors in these files:\n{}\n'.format(err_files_str)
            warnings.warn(message, category=Wien2kWarning)

    def __repr__(self):
        return 'Wien2kParse({})'.format(op.relpath(self.case_path))

    def get_file_path(self, filetype):
        path = op.join(self.case_path, self.case + '.' + filetype)

        return path

if __name__ == '__main__':
    path = 'data/raw/TaP/'
    TaP = Wien2kParse(path)
#    NaNO2 = Wien2kParse('data/raw/NaNO2')

