import warnings

def warning_message(message, category, filename,
                        lineno, file=None, line=None):
        return '{}:{}: {}:\n{}\n'.format(filename, lineno,
                                         category.__name__, message)

warnings.formatwarning = warning_message

class Wien2kWarning(Warning):
    pass

