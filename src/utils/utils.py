#!/usr/bin/env python
from monty.io import zopen
import os.path as op

ROOT = op.abspath(op.join(__file__, '../../../'))

# Custom regex patterns
def custom_regex_patterns(kind):
    patterns = {'number': r'-?\d+(\.\d*)?',
                'float': '-?\d+\.\d+'}

    return patterns[kind]

def load_file(file_path):
    try:
        with zopen(file_path, 'rt') as f:
            text = f.read()
        return text
    except FileNotFoundError:
        pass

    try:
        with zopen(file_path + '.gz', 'rt') as f:
            text = f.read()
        return text
    except FileNotFoundError:
        pass

if __name__ == '__main__':
    text = load_file('src/utils.py')
    print(text)
